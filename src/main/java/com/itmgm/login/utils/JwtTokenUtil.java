package com.itmgm.login.utils;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.itmgm.login.security.JwtTokenProvider;


@Component
public class JwtTokenUtil {

	@Autowired
	public AuthenticationManager authenticationManager;

	@Autowired
	public JwtTokenProvider tokenProvider;

	public Authentication createToken(String usernameOrEmail, String password) {

		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(usernameOrEmail, password));

		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		return authentication;
	}

	public String generateToken(Authentication authentication) throws Exception {
		String jwt ="";
		try {
			jwt = tokenProvider.generateToken(authentication);
		} catch (IOException e) {
			throw new Exception("no se puede generar token");
		}
		return jwt;
	}
	
}

