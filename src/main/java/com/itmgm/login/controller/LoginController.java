package com.itmgm.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itmgm.login.dto.LoginRequestDto;
import com.itmgm.login.dto.LoginResponseDto;
import com.itmgm.login.service.LoginService;
import com.itmgm.login.utils.ConstantesUtil;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST })
@RequestMapping("/api/itmgm/v01/common")
public class LoginController {

	@Autowired
	public LoginService loginService;

	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody LoginRequestDto loginDTO) throws Exception {

		LoginResponseDto auth = null;

		try {
			auth = loginService.sigin(loginDTO);
		} catch (Exception e) {
			if (e.getMessage() == ConstantesUtil.MSG_BAD_CRED) {
				return new ResponseEntity<String>(ConstantesUtil.MSG_LOGIN_ERRONEO, HttpStatus.BAD_REQUEST);
			} else {
				throw new Exception(e.getMessage());
			}
		}

		return new ResponseEntity<>(auth, auth == null ? HttpStatus.NO_CONTENT : HttpStatus.OK);
	}
}
