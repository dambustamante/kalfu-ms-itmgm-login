package com.itmgm.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.itmgm.login.model.MgmUsuarioModel;

@Repository
public interface MgmUsuariosRepository extends JpaRepository<MgmUsuarioModel, Long> {

	MgmUsuarioModel findByUsuarioEmail(String usuarioEmail);
}
