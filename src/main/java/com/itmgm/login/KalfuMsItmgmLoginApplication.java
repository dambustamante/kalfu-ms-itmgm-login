package com.itmgm.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KalfuMsItmgmLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(KalfuMsItmgmLoginApplication.class, args);
	}

}
