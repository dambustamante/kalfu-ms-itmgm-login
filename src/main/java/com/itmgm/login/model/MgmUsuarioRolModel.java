package com.itmgm.login.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "MGM_USUARIO_ROL")
public class MgmUsuarioRolModel implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MGMUSR_USUARIOROL_ID")
    private Long usuarioRolId;

	@Column(name = "MGMUSR_USUARIOROL_ROL_ID")
    private Long usuarioRolRolId;
	
	@Column(name = "MGMUSR_USUARIOROL_USUARIO_ID")
    private Long usuarioRolUsuarioId;
	
    @Column(name = "MGMUSR_USUARIOROL_FECHA_CREACION")
    private Date usuarioRolFechaCreacion;
    
    @Column(name = "MGMUSR_USUARIOROL_FECHA_MODIFICACION")
    private Date usuarioRolFechaModificacion;

	public Long getUsuarioRolId() {
		return usuarioRolId;
	}

	public void setUsuarioRolId(Long usuarioRolId) {
		this.usuarioRolId = usuarioRolId;
	}

	public Long getUsuarioRolRolId() {
		return usuarioRolRolId;
	}

	public void setUsuarioRolRolId(Long usuarioRolRolId) {
		this.usuarioRolRolId = usuarioRolRolId;
	}

	public Long getUsuarioRolUsuarioId() {
		return usuarioRolUsuarioId;
	}

	public void setUsuarioRolUsuarioId(Long usuarioRolUsuarioId) {
		this.usuarioRolUsuarioId = usuarioRolUsuarioId;
	}

	public Date getUsuarioRolFechaCreacion() {
		return usuarioRolFechaCreacion;
	}

	public void setUsuarioRolFechaCreacion(Date usuarioRolFechaCreacion) {
		this.usuarioRolFechaCreacion = usuarioRolFechaCreacion;
	}

	public Date getUsuarioRolFechaModificacion() {
		return usuarioRolFechaModificacion;
	}

	public void setUsuarioRolFechaModificacion(Date usuarioRolFechaModificacion) {
		this.usuarioRolFechaModificacion = usuarioRolFechaModificacion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
