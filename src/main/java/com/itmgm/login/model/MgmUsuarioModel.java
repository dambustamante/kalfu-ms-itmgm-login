package com.itmgm.login.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "MGM_USUARIOS")
public class MgmUsuarioModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MGMUS_USUARIO_ID")
    private Long usuarioId;
	
	@ManyToMany(cascade = {
	        CascadeType.PERSIST,
	        CascadeType.MERGE
	    })
	    @JoinTable(name = "MGM_USUARIOS_ROL",
	        joinColumns = @JoinColumn(name = "MGMUSR_USUARIOROL_USUARIO_ID"),
	        inverseJoinColumns = @JoinColumn(name = "MGMUSR_USUARIOROL_ROL_ID")
	    )
	private List<MgmRolesModel> rolesList = new ArrayList<>();
	
	@Column(name = "MGMUS_USUARIO_EMAIL")
    private String usuarioEmail;

    @Column(name = "MGMUS_USUARIO_CONTRASENA")
    private String usuarioPassword;
    
    @Column(name = "MGMUS_USUARIO_ESTADO")
    private int usuarioEstado;
    
    @Column(name = "MGMUS_PERSONA_ID")
    private Long personaId;
    
    @Column(name = "MGMUS_USUARIO_FECHA_CREACION")
    private Date usuarioFechaCreacion;
    
    @Column(name = "MGMUS_USUARIO_FECHA_MODIFICACION")
    private Date usuarioFechaModificacion;

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public List<MgmRolesModel> getRolesList() {
		return rolesList;
	}

	public void setRolesList(List<MgmRolesModel> rolesList) {
		this.rolesList = rolesList;
	}

	public String getUsuarioEmail() {
		return usuarioEmail;
	}

	public void setUsuarioEmail(String usuarioEmail) {
		this.usuarioEmail = usuarioEmail;
	}

	public String getUsuarioPassword() {
		return usuarioPassword;
	}

	public void setUsuarioPassword(String usuarioPassword) {
		this.usuarioPassword = usuarioPassword;
	}

	public int getUsuarioEstado() {
		return usuarioEstado;
	}

	public void setUsuarioEstado(int usuarioEstado) {
		this.usuarioEstado = usuarioEstado;
	}

	public Long getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Long personaId) {
		this.personaId = personaId;
	}

	public Date getUsuarioFechaCreacion() {
		return usuarioFechaCreacion;
	}

	public void setUsuarioFechaCreacion(Date usuarioFechaCreacion) {
		this.usuarioFechaCreacion = usuarioFechaCreacion;
	}

	public Date getUsuarioFechaModificacion() {
		return usuarioFechaModificacion;
	}

	public void setUsuarioFechaModificacion(Date usuarioFechaModificacion) {
		this.usuarioFechaModificacion = usuarioFechaModificacion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
}
