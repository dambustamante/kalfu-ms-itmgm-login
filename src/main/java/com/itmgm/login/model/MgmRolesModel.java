package com.itmgm.login.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name = "MGM_ROLES")
public class MgmRolesModel implements Serializable  {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MGMRO_ROL_ID")
    private Long rolId;
	
	@ManyToMany(mappedBy = "rolesList")
	private Set<MgmUsuarioModel> usuariosList = new HashSet<>();

    @Column(name = "MGMRO_ROL_FECHA_CREACION")
    private Date rolFechaCreacion;

	@Column(name = "MGMRO_ROL_FECHA_MODIFICACION")
    private Date rolFechaModificacion;

	@Column(name = "MGMRO_ROL_DESCRIPCION", length=100)
    private String rolDescripcion;

	public Long getRolId() {
		return rolId;
	}

	public void setRolId(Long rolId) {
		this.rolId = rolId;
	}

	public Set<MgmUsuarioModel> getUsuariosList() {
		return usuariosList;
	}

	public void setUsuariosList(Set<MgmUsuarioModel> usuariosList) {
		this.usuariosList = usuariosList;
	}

	public Date getRolFechaCreacion() {
		return rolFechaCreacion;
	}

	public void setRolFechaCreacion(Date rolFechaCreacion) {
		this.rolFechaCreacion = rolFechaCreacion;
	}

	public Date getRolFechaModificacion() {
		return rolFechaModificacion;
	}

	public void setRolFechaModificacion(Date rolFechaModificacion) {
		this.rolFechaModificacion = rolFechaModificacion;
	}

	public String getRolDescripcion() {
		return rolDescripcion;
	}

	public void setRolDescripcion(String rolDescripcion) {
		this.rolDescripcion = rolDescripcion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MgmRolesModel() {
		
    }	    
}
