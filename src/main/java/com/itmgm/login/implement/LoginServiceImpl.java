package com.itmgm.login.implement;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.itmgm.login.dto.LoginRequestDto;
import com.itmgm.login.dto.LoginResponseDto;
import com.itmgm.login.security.UserPrincipal;
import com.itmgm.login.service.LoginService;
import com.itmgm.login.utils.JwtTokenUtil;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	public JwtTokenUtil jwtTokenUtil;

	@Override
	public LoginResponseDto sigin(LoginRequestDto loginDTO) throws Exception {

		Authentication sigin = null;
		LoginResponseDto loginResponseDto = null;

		try {
			sigin = jwtTokenUtil.createToken(loginDTO.getUsuarioEmail(), loginDTO.getUsuarioPassword());
			loginResponseDto = MapeoDatosLogin(sigin);

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

		return loginResponseDto;
	}

	private LoginResponseDto MapeoDatosLogin(Authentication sigin) throws Exception {

		UserPrincipal principal = (UserPrincipal) sigin.getPrincipal();
		LoginResponseDto response = new LoginResponseDto();

		response.setData(Map.of("usuarios", Map.of("usuarioId", principal.getIdUsuario(), "usuarioEmail",
				principal.getEmail(), "usuarioToken", jwtTokenUtil.generateToken(sigin))));

		return response;
	}

}
