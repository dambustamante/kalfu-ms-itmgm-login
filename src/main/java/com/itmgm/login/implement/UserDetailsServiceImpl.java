package com.itmgm.login.implement;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itmgm.login.model.MgmUsuarioModel;
import com.itmgm.login.repository.MgmUsuariosRepository;
import com.itmgm.login.security.UserPrincipal;
import com.itmgm.login.utils.ConstantesUtil;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
    
	@Autowired
    private MgmUsuariosRepository mgmUsuariosRepository;

    @Override
    @Transactional(readOnly = true)
    public UserPrincipal loadUserByUsername(String usuarioEmail){
    	
    	MgmUsuarioModel user = mgmUsuariosRepository.findByUsuarioEmail(usuarioEmail);
        
    	if (user == null) throw new UsernameNotFoundException(ConstantesUtil.MSG_LOGIN_ERRONEO);
        
        return createUserPrincipal(user);
    }
    
    private UserPrincipal createUserPrincipal(MgmUsuarioModel usuarioModel){

		UserPrincipal userPrincipal = new UserPrincipal();

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(usuarioModel.getRolesList().get(0).getRolDescripcion()));
        
        userPrincipal.setIdUsuario(usuarioModel.getUsuarioId());
		userPrincipal.setAuthorities(grantedAuthorities);
		userPrincipal.setPassword(usuarioModel.getUsuarioPassword());
		userPrincipal.setEmail(usuarioModel.getUsuarioEmail()); 

		return userPrincipal;
	}
}
