package com.itmgm.login.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class LoginRequestDto {

	@JsonInclude(Include.NON_NULL)
	private String usuarioEmail;
	
	@JsonInclude(Include.NON_NULL)
	private String usuarioPassword;
	
	public String getUsuarioEmail() {
		return usuarioEmail;
	}
	public void setUsuarioEmail(String usuarioEmail) {
		this.usuarioEmail = usuarioEmail;
	}
	public String getUsuarioPassword() {
		return usuarioPassword;
	}
	public void setUsuarioPassword(String usuarioPassword) {
		this.usuarioPassword = usuarioPassword;
	}
	
	public LoginRequestDto(String usuarioEmail, String usuarioPassword) {
		super();
		this.usuarioEmail = usuarioEmail;
		this.usuarioPassword = usuarioPassword;
	}
}
