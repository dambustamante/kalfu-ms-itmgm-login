package com.itmgm.login.dto;

import java.util.List;
import java.util.Map;

public class LoginResponseDto {

	private Map<String, Map<String, Object>> data;
	private String message;
	private List<String> errors;


	public Map<String, Map<String, Object>> getData() {
		return data;
	}

	public void setData(Map<String, Map<String, Object>> data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public List<String> getErrors() {
		return errors;
	}
	
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	
}
