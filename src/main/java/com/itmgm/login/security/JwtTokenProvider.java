package com.itmgm.login.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.jackson2.SimpleGrantedAuthorityMixin;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenProvider {

	private String jwtSecret ="PR-SOS-SECRET";
	
	public String generateToken(Authentication authentication) throws IOException {
		
		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
		Claims claims = Jwts.claims();
		
		Date expiryDate = new Date(System.currentTimeMillis() + 6000000);

		return Jwts.builder().setClaims(claims).setSubject(userPrincipal.getEmail().toString())
				.setIssuedAt(new Date()).setExpiration(expiryDate).signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}

	public Long getUserIdFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
		return Long.parseLong(claims.getSubject());
	}

	public Collection<? extends GrantedAuthority> getPermisos(String token) throws IOException {
		String permisos = getDataByKeyClaims("authorities", token);
	
		Collection<GrantedAuthority> authorities = new ArrayList<>();

		if(permisos != null && !permisos.isEmpty()) {
			authorities = Arrays
				.asList(new ObjectMapper().addMixIn(SimpleGrantedAuthority.class, SimpleGrantedAuthorityMixin.class)
						.readValue(permisos.getBytes(), SimpleGrantedAuthority[].class));
		}

		return authorities;
	}

	public Claims getClaims(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
		return claims;
	}

	private String getDataByKeyClaims(String keyClaimsTokenEnum, String token) {
		Claims claims = getClaims(token);

		return findKeyClaimsInData(keyClaimsTokenEnum, claims);
	}

	private String findKeyClaimsInData(String keyClaimsTokenEnum, Claims claims) {
		return claims.get(keyClaimsTokenEnum, String.class);
	}
}
